package SpaceInvadersPackage;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javafx.scene.paint.Color;

public class GameEntityTest {
	GameEntity player;
	GameEntity bullet;
	GameEntity enemy;
	private double epsilon = 0.000001d;
	@BeforeEach
	public void setup() {
		player = new GameEntity("player",250,150, 30, 30,null);
		bullet = new GameEntity("bullet",250,100, 5, 10,Color.BLACK);
		enemy = new GameEntity("enemy",250,50, 30, 30,Color.BLACK);
	}
	@Test
	public void testConstructorExpectedArguments() {
		assertEquals(250d, player.getTranslateX(),epsilon);
		assertEquals(250d, enemy.getTranslateX(),epsilon);
		assertEquals(100d, bullet.getTranslateY(),epsilon);
		assertEquals(100d, bullet.getPosY(),epsilon);//tester denne og fordi det er en funksjon i koden
		assertEquals("player", player.getType());
		assertEquals("enemy", enemy.getType());
		
		}
	@Test
	public void testConstructorWithIllegalArguments() {
		assertThrows(IllegalArgumentException.class, ()->new GameEntity("player", 1000, 1, 32, 32, null));
		assertThrows(IllegalArgumentException.class, ()->new GameEntity("Enemy", -10, 1, 32, 32, Color.BLACK));
		assertThrows(IllegalArgumentException.class, ()->new GameEntity("Bullet", 10, 1, 501, 32, Color.BLACK));
		assertThrows(IllegalArgumentException.class, ()->new GameEntity("player", 100, 1, 601, 32, Color.BLACK));
		assertThrows(IllegalArgumentException.class, ()->new GameEntity("player", 100, 1000, 32, 32, Color.BLACK));

	}
	@Test
	public void testMoves() {
		player.moveRight();
		assertEquals(255d, player.getTranslateX(),epsilon);
		player.moveLeft();
		player.moveLeft();
		assertEquals(245d, player.getTranslateX(),epsilon);
	
		bullet.moveDown();
		assertEquals(105d, bullet.getTranslateY(),epsilon);
	}
	@Test
	public void typeTester() {
		assertTrue(player.getType().equals("player"));
		assertTrue(enemy.getType().equals("enemy"));
		assertTrue(bullet.getType().equals("bullet"));
	}
	@Test
	public void deadOrAliveTest() {
		assertFalse(player.isDead());
		assertFalse(bullet.isDead());
		enemy.kill();
		assertTrue(enemy.isDead());
		int health =player.getHealth();
		player.chopHealth();
		assertEquals(health-1, player.getHealth());
	}
	@Test
	public void extremePlayerMoves() {
		for (int i = 0; i < 100; i++) {
			player.moveLeft();
		}
		assertEquals(2d, player.getTranslateX(),epsilon);
		//to fordi det ser bra ut å ha to piksler mellom spiller og kanten av skjermen
		for (int i = 0; i < 100; i++) {
			player.moveRight();
		}
		assertEquals(500-32, player.getTranslateX(),epsilon);
	}
}
