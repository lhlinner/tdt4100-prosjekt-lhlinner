package SpaceInvadersPackage;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AppController {
	@FXML private Label whoPlaysLabel;
	@FXML private TextArea informationTextArea;
	@FXML private TextField usernameTextField;
	
	//har begge scensene i cobntrolleren for å enkelt bytte scene istedet for å gå via spillet
	private Scene gameScene,menuScene;//vil si at det også gjør det mer i henhold til MVC, at controller bestemmer view istedet for model
	private Stage theStage;//her THE stage for å hjelpe meg selv underveis at det kun er en stage men flere scenes
	private SpaceInvadersApp spaceInvadersApp;//ved å lage denne relasjonen mellom controlleren og spillet blir det betraktelig enklere å kalle på hverandre
	private ScoreboardHandler ScoreboardHandler;
	private String topThree;

///////// FXML metoder//////////////////////////////////////////////////////////////////////////////////////////////
	@FXML
	public void initialize() {
		ScoreboardHandler = new ScoreboardHandler();//initialiserer en scoreboardhandler
		topThree = ScoreboardHandler.getTopTreeText();//skaffer top tre tekst
		informationTextArea.setText(topThree);//printer top tre til bruker i en textboks
	}
	
	@FXML
	private void onPlayBtn(ActionEvent event) throws Exception {
		System.out.println("toggled Play");
		spaceInvadersApp.unPauseGame();
		switchToGameScene();
	}

	@FXML
	private void onExitBtn() {
		ScoreboardHandler.saveResource("Scoreboard");//lagrer scoreboard når spillet avsluttes
		System.out.println("toggled Exit");
		System.exit(0);//terminater programmet
	}
	@FXML
	private void onToggleUsername() {//knappen til høyre for hvor man skrive inn navnet
		System.out.println("toggled toggle username btn");
		ScoreboardHandler.registerUser(usernameTextField.getText());//leser inn navnet brukeren har skrevet å sjekker dette opp i mot scoreboardhandler
		informationTextArea.setText(" Welcome "+usernameTextField.getText() + "! \n" + topThree +" \n" + ScoreboardHandler.getBestRunText());//skrive en velkomstmelling til spiller
	}
///////// Metoder for å sette opp kontrolleren slik at jeg kan bytte mellom scenes ///////////////////////////////////
	public void setTheStage(Stage stage) {
		this.theStage = stage;
	}
	public void setGame(SpaceInvadersApp s) {
		this.spaceInvadersApp=s;
	}
/////////Metoder for å bytte mellom scenes ////////////////////////////////////////////////////////////////////////////
	private void switchToGameScene() {//funksjon for å gå fra meny til spill
		System.out.println("toggled switch to gameScene");
		this.gameScene = spaceInvadersApp.getScene();//kan desverre ikke være i initialize fordi da er scene null
		this.theStage.setScene(gameScene);
		this.theStage.show();
	}
		
 	public void switchToMenuScene(){//funksjon for å gå fra spill til meny
 		System.out.println("toggled switch to gameScene");
 		menuScene = whoPlaysLabel.getScene();//henter scenen enkelt gjennom et tilfeldig fxml felt istedet for å bruke fxml-loader
 		theStage.setScene(menuScene);
		}
/////////Metoder for scoreboard handler ////////////////////////////////////////////////////////////////////////////	
	public void fireWaveCount(int WaveCount) {
		ScoreboardHandler.updateUserWave(usernameTextField.getText(), WaveCount);
		informationTextArea.setText(" Having Fun "+usernameTextField.getText() + "? \n" + topThree +" \n" + ScoreboardHandler.getBestRunText());
	}//dette er det mest passende stedet dette kallet kan være selv om det kalles flere ganger enn nødvendig
	

	
}
