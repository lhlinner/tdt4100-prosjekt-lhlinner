package SpaceInvadersPackage;

import java.util.HashMap;

import javafx.scene.image.Image;

public class SpriteHandler implements FileHandler{
	
	//holder orden over hvilke filer som skal lastes inn i dette arrayet og lagres bildene med navn i hasmappet
	private String[] aviablePicturesArray = {"Bullet","Enemy","FullHealth","TwoThirdsHealth","OneThirdHealth","TDTSpaceShip","SpaceBackgrund","EmptyHealth"};
	private HashMap<String, Image> spritesMap = new HashMap<>();

	// Metoder ////////////////////////////////////////
	public SpriteHandler() {//konstruktøren laster inn alle bilder med en gang
		for (String picture : aviablePicturesArray) {
			try {
				loadGameResource(picture);
			}
			catch (IllegalArgumentException e) {
				System.out.println("// Spritehandler load game resource failed due to wrong file name // ");
				e.printStackTrace();
			}
			catch (Exception e) {
				System.out.println("// Spritehandler load game resource failed due to unknown error // ");
				e.printStackTrace();
			}
		}
	}
	
	@Override//kunne vært private men er public fra interfacet så den forblir public
	public void loadGameResource(String filename) throws IllegalArgumentException{//ja illegal Argumentexception fordi new Image(url) kaster denne typen argument
		String fullPath = "SpaceInvadersPackage/"+filename+".png";
		Image sprite = new Image(fullPath);			
		storeInMap(filename, sprite);//legger bildet i hashmappet med key
	}
	private void storeInMap(String name, Image sprite) {
		spritesMap.put(name, sprite);
	}
	
	
	public Image getSprite(String name){//funksjon for å hente ut en valgfri sprite
		if (spritesMap.containsKey(name)) {//validerer at spriten finnes
			return spritesMap.get(name);	
		}
		else {
			throw new IllegalArgumentException("// That sprite does not exist \nSpritehandler.java getSprite function //");
		}
	}
	//gir ikke mening å lagre noe her
	@Override
	public void saveResource(String filename) {
		return;
	}

}
