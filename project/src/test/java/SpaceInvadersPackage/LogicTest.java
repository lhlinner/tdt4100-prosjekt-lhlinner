package SpaceInvadersPackage;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javafx.scene.paint.Color;

public class LogicTest {
	SpaceInvaderLogic logic;
	GameEntity enemyBullet, playerBullet, player;
	List<GameEntity> enemies = new ArrayList<>();
	
	
	@BeforeEach
	public void setUp(){
		logic = new SpaceInvaderLogic();
		player = new GameEntity("player", 250, 20, 32, 32, Color.WHITE);
		enemyBullet = new GameEntity("enemybullet", 250, 0, 10, 10, Color.BLACK);//pos 0 som er ti piksler over player
		enemies.add(new GameEntity("enemy", 250, 10, 32, 32, Color.RED));
		enemies.add(new GameEntity("enemy", 300, 10, 32, 32, Color.RED));
		playerBullet = new GameEntity("playerbullet", 250, 52, 10, 10, Color.WHITE);//pos 52 som er 10 piksler under en enemy
	}
	
//tester for enemybullet();	
	@DisplayName (value = "Teste at spilleren ikke blir flyttet av funksjonen")
	@Test
	public void EnemyBulletTestZero() {
		int originalPosX = (int)player.getPosX();
		int originalPosY = (int)player.getPosY();
		logic.enemyBullet(enemyBullet, player);
		assertEquals(originalPosX, (int)player.getPosX());
		assertEquals(originalPosY, (int)player.getPosY());
	}
	@DisplayName (value = "Teste at det ikke skjer noe når kula ikke er inntil spilleren")
	@Test
	public void EnemyBulletTestOne() {
		logic.enemyBullet(enemyBullet, player);//kula står i pos y=5 som vil si at den når y=15
		assertFalse(player.isDead());// player befinner seg på y=20 og det burde ikke
		assertFalse(enemyBullet.isDead());//ha skjedd noe annet enn endring i posisjonen til kula
		assertEquals(3, player.getHealth());
		assertEquals(5, enemyBullet.getPosY());//påser at kula faktisk flytta seg
		
	}
	@DisplayName (value = "Teste at det skjer noe når kula kommer inntil spilleren")
	@Test
	public void EnemyBulletTestTwo() {
		enemyBullet.moveDown();//nå står kula i posY 5, nestegang moveDown() kalles vil den stå på 10 og da
		logic.enemyBullet(enemyBullet, player);//vil kanten på kula (10x10) intersecte med player
		assertFalse(player.isDead());//spilleren har tre liv og burde derfor ikke være død
		assertEquals(2, player.getHealth());//nå skal spilleren ha mistet ett liv
		assertTrue(enemyBullet.isDead());// nå har bulleten toucha en gang og burde være død
	}
	@DisplayName (value = "Teste at spilleren blir drept av tre kuler")
	@Test
	public void EnemyBulletTestThree() {
		for (int i = 0; i < 3; i++) {
			enemyBullet = new GameEntity("enemubullet", 250, 5, 10, 10, Color.BLACK);
			logic.enemyBullet(enemyBullet, player);
			assertEquals(2-i, player.getHealth());
		}
		assertTrue(player.isDead());
	}
	@DisplayName (value = "Teste at kula dør etter den beveger seg utenfor spillet")
	@Test
	public void EnemyBulletTestFour() {
		enemyBullet = new GameEntity("enemubullet", 250, 591, 10, 10, Color.BLACK);
		logic.enemyBullet(enemyBullet, player);
		assertFalse(enemyBullet.isDead());//kula skal befinne seg på 596 og burde ikke være død
		logic.enemyBullet(enemyBullet, player);
		assertEquals(601, enemyBullet.getPosY());
		assertTrue(enemyBullet.isDead());//kula befinner seg på 601 og burde være død
	}
//tester for playerbullet();
	@DisplayName (value = "Teste at enemies ikke beveger på seg")
	@Test
	public void PlayerBulletTestOne() {
		assertEquals(52, playerBullet.getPosY());//konfirmerer startposisjonen satt i setup
		logic.playerBullet(playerBullet, enemies);//kaller på funksjonen vi tester to ganger
		assertEquals(47, playerBullet.getPosY());//påser at kula flyttet seg
		assertEquals(10, enemies.get(0).getPosY());//enemies står i ro
		assertEquals(10, enemies.get(1).getPosY());
		assertEquals(250, enemies.get(0).getPosX());
		assertEquals(300, enemies.get(1).getPosX());
	}
	@DisplayName (value = "Teste at det ikke skjer noe når kula ikke er inntil fienden")
	@Test
	public void PlayerBulletTestTwo() {
		logic.playerBullet(playerBullet, enemies);//kaller på funksjonen vi tester
		assertEquals(47, playerBullet.getPosY());//påser at kula flyttet seg
		assertFalse(enemies.get(0).isDead());// kula og fiende burde ikke være døde siden de ikke
		assertFalse(playerBullet.isDead());//intersecter enda
	}
	@DisplayName (value = "Teste at det skjer noe når kula kommer inntil fienden")
	@Test
	public void PlayerBulletTestThree() {
		logic.playerBullet(playerBullet, enemies);//kaller på funksjonen vi tester to ganger
		logic.playerBullet(playerBullet, enemies);
		assertEquals(42, playerBullet.getPosY());//påser at kula flyttet seg til intersect posisjon
		assertTrue(enemies.get(0).isDead());// kula og fiende burde ikke være døde siden de ikke
		assertTrue(playerBullet.isDead());//intersecter enda
	}
	@DisplayName (value = "Teste at kula dør når den kommer utenfor spillet")
	@Test
	public void PlayerBulletTestFour() {
		playerBullet = new GameEntity("playerBullet", 200, 9, 10, 10, Color.BLACK);
		logic.playerBullet(playerBullet, enemies);//posisjonen er nå 4
		assertFalse(playerBullet.isDead());//da burde kula ikke være død
		logic.playerBullet(playerBullet, enemies);//posisjonen er nå -1
		assertTrue(playerBullet.isDead());//da burde kula dø
	}

/*  får ikke testet next wave, fordi den kaller til slutt på appen som ikke er launcha
 * og jeg får en nullpointerexcedption.
 * får til å launche en app herifra men problemet er at jeg ikke får tak i app-objektet etterpå
 * SpaceInvadersApp app =Application.launch(SpaceInvadersApp.class, null);
 * går ikke fordi høyresiden er void, har også prøvd
 * SpaceInvadersApp app;
 * 		Application.launch(app.getClass(), null);
 * med en rekke variasjoner men får det ikke til å funke
 */

	
}
