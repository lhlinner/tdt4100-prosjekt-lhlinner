package SpaceInvadersPackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ScoreboardHandler implements FileHandler{//implementer grensesnittet jeg har lagd
	private int tempRankInt =0,CurrentUserBestRun;//temp rank int er for å hjelpe til med å returnere top three text
	private String prettyPrint = "Top three players \nRank|Wave|Name \n";//også for å returnere top three text
	private HashMap<String, Integer> scoreboard = new HashMap<>();//intern tilstand over alle som noen gang har spilt spillet og dynamisk mulighet til å legge til fler
	private final String defaultFileName = "scoreboard";//filnavnet der jeg normalt lagrer tilstanden
	
	public ScoreboardHandler() {
		this.loadGameResource(defaultFileName);//ved å ha denne konstruktøren blir denne klassen ganske automatisk jeg slipper unna en del funksjonskall fra controlleren
	}
	
	@Override
	public void loadGameResource(String filename) {//funksjon fra grensesnittet som henter inn lagret tilstand
		System.out.println("load game resource was toggled ");
		try {//bruker en buffered reader fordi fila er liten og det koster ikke noe særlig å ha den buffret
			BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/SpaceInvadersPackage/"+filename+".txt"));
			String line;
			while ((line = reader.readLine()) != null) {//leser fila linje for linje inn i hashmappet
				String[] splitLine = line.split(",");// splitter på "," fordi fromatet er på wave,navn
				scoreboard.put(splitLine[1], Integer.parseInt(splitLine[0]));//konvertere fra "tall" til tall 
			}//__________________^navn_____________________________^wave_____
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("// Read text from file function failed (file not found Exception)//");
			e.printStackTrace();		
		} catch (IOException e) {
			System.out.println("// Read text from file function failed (IOexception)//");
			e.printStackTrace();
		}
	}
	
	public String getTopTreeText() {//en funksjon for å skrive en pent formattert tekst til brukeren gjennom tekstområdet
		List<String> topTreeNames = scoreboard.entrySet().stream()//strømmer gjennom hashmappet med informasjonen om alle spillere
				.sorted(Map.Entry.<String,Integer>comparingByKey())//et forsøk på å få det noenlunde i alfabetisk rekkefølge
				.sorted(Map.Entry.<String, Integer>comparingByValue().reversed())//sorter på høyest wavecount
				.limit(3)//vi er bare interessert i de tre beste
				.map(Map.Entry::getKey)//må mappe det til en liste for å kunne gjøre noe med det i etterkant
				.collect(Collectors.toList());
		topTreeNames.forEach(e ->{//lager en pent formattert tekst som passer til teksområdet og returnerer det
			tempRankInt++;
			prettyPrint += (" " + String.valueOf(tempRankInt) + ".        "+String.valueOf(scoreboard.get(e))+ "      " + e + "\n");
		});//hadde sikkert vært lurt å bruke formatert text men det får bli en annen dag		
		return prettyPrint;
	}
	@Override
	public void saveResource(String filename) {//funksjon for å lagre tilstand til fil fra interfacet
		System.out.println("save scoreboard was toggled");
		try {//bruker en buffered writer i og med at fila er liten og det er lite koding å gjøre det slik
			BufferedWriter write = new BufferedWriter(new FileWriter("src/main/resources/SpaceInvadersPackage/"+filename+".txt"));//opretter fila
			scoreboard.entrySet().forEach(entry -> {//strømmer gjennom hasmappet og lagret hver key value
				String tempString =String.valueOf(entry.getValue())+","+entry.getKey();//lager en string av key og tilhørende value
				//System.out.println(tempString);
				try {
					write.write(tempString);//skriver det til fila + ny linje så vi ikke overskriver hver linje og ender opp med ingenting lagret
					write.newLine();
				} catch (IOException e) {
					System.out.println("// saveGameState function 2. try/catch (IO Exception) //");
					e.printStackTrace();
				}
			});
			write.close();
		} catch (IOException e) {
			System.out.println("// save scorebboeard function failed (IO Exception) // ");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("// save scoreboard function failed (Unknown Exception)//");
			e.printStackTrace();		
		}
	}
	
	public void registerUser(String username) {//funksjon for å registrere en ny spiller som ikke har spilt før
		if ((username == null) || (username=="")) {
			return;
		}
		if (scoreboard.containsKey(username)) {
			CurrentUserBestRun = scoreboard.get(username);//om spilleren finnes fra før trenger vi ikke registrere den en gang til og setter current best run til hva den har oppnådd før
		}
		else { 
			CurrentUserBestRun = 0;//om brukeren ikke har spilt før setter vi current best run til 0 og legger til navnet i den interne tilstanden(hashmappet)
			scoreboard.put(username, CurrentUserBestRun);
		}
	}
	public void updateUserWave(String username,int wave) {//funksjonn for å oppdatere nivået en bruker har nådd
		if ((username == null) || (username=="")) {//feilhåndtering om vi får inn ingenting
			return;
		}
		if (scoreboard.containsKey(username)) {	//feilhåndtering om vi på magisk vis skulle prøve å oppdatere wavecount på en bruker som ikke finnes
			if (CurrentUserBestRun<wave) {
				this.CurrentUserBestRun=wave;
				scoreboard.put(username, wave);
			}
		} else {
			registerUser(username);
			updateUserWave(username, wave);
		}
		
	}
	public String getBestRunText() {//getterfunksjon for controlleren slik at den kan hente tekst til tekstområdet
		try {
			return ("your record is wave: "+String.valueOf(CurrentUserBestRun)+"\nSICK!");
			
		} catch (Exception e) {
			System.out.println("// get best run text function failed because wave is null //");
			return "no record wave";
		}
	}
	
	
//funksjoner for å kunne bruke testkode///////
	public boolean checkIfScoreboardLoadedToHashmap(String key) {//dette er funksjoner kun for å kunne bruke enhetstester
		return scoreboard.containsKey(key);
	}
	public void clearScoreboard() {
		this.scoreboard.clear();
	}
}
