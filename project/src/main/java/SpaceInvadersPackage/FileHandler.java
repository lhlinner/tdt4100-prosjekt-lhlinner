package SpaceInvadersPackage;

public interface FileHandler {
	
	void loadGameResource(String filename) throws Exception;
	void saveResource(String filename);
}
