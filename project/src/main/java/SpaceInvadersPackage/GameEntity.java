package SpaceInvadersPackage;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class GameEntity extends Rectangle{
	private double posY,posX;//holder oversikt over posisjonen mtp feilhåndtering
	private final int gameHeight=600, gamewidth=500;//oversikt over størrelsen på skjermen
	private boolean dead = false;
	private final String type;// oversikt over hva slags entity det er, spiller enemy bullet neutral
	private int health;
	//konstruktør som tar inn type, posisjon, størrelse og farge
	public GameEntity(String type, double x, double y, int width, int height,Color color) {
		super(width,height,color);
		if (x <0 || x>gamewidth ||y <0 ||y>gameHeight || width >gamewidth || height > gameHeight) {//påser at inputverdiene er rimelige
			throw new IllegalArgumentException();
		}
		this.type=type;
		this.posX=x;
		this.posY=y;
		setTranslateX(x);//faktisk gi rektangelet en posisjon
		setTranslateY(y);
		if (type.equals("player")) {//om typen er av Spiller så skal den ha liv
			this.health = 3;
		}
	}
	public int getHealth() {//getter for health
		return health;
	}
	public void chopHealth() {//tar et liv fra health
		if (this.type == "player") {
			this.health--;			
		}

	}
	public void kill() {//setter tilstanden dead til true, dead er en boolean
		this.dead =true;
	}
	public boolean isDead() {//boolsk funksjon som returnerer om enheten er død eller ei 
		if (dead) {
			return true;			
		}
		return false;
	}
	public String getType() {//getter for typen
		return type;
	}
	public double getPosY() {//getter for y posisjon
		return posY;
	}
	public double getPosX() {//getter for x possjon, brukes kun av testene foreløpig
		return posX;
	}
	public void moveRight() {//funksjon for å bevege enheten mot høyre, brukes foreløpig kun på player
		setTranslateX(getTranslateX()+5);
		posX+=5;//oppdaterer posx verdien for å holde orden på at vi ikke kan bevege oss utenfor kartet i x retnign
		if (posX > gamewidth-32) {
			posX = gamewidth-32;
			setTranslateX(posX);
		}
	}
	public void moveLeft() {//det samme gjelder denne funksjonen bare for å bevege seg mot venstre
		setTranslateX(getTranslateX()-5);
		posX-=5;
		if (posX < 2) {//bruker 2 og ikke 0 fordi det ser bra ut med litt spacing mellom spiller og ramma
			posX = 2;
			setTranslateX(posX);
		}
	}
	public void moveUp() {//funksjon for å bevege enhet opp, brukes foreløpig kun på bullets
		setTranslateY(getTranslateY()-5);
		posY-=5;//holder orden på hvor den er internt så man kan bruke getPosY, man kunne såklart brukt funksjonen getTranslateX i andre funksjonskall, men like greit å bare holder orden selv
	}
	public void moveDown() {//det samme gjelder denne bare ned
		setTranslateY(getTranslateY()+5);
		posY+=5;
	}
}
