package SpaceInvadersPackage;

import java.util.List;

public class SpaceInvaderLogic {
	
	private double time=0;//holder oversikten over tid for at å tilfeldig skyte kuler fra fiendene mot spilleren
	private final int gameHeight = 600,gameWidth = 500;//skjermstørrelse er kjekk å ha så jeg ikke trenger å huske på den og det går fortere om jeg ønsker å endre den senere
	private int waveCount = 0;//oversikt over hvilket nivå spillerten har nådd
	private SpaceInvadersApp spcApp;//en instans av appen for å enkelt kunne kalle på den
	

	public void tick(GameEntity player,AppController appController) {//dette er hjernen i spillet, all logikk er samlet her
		time += 0.016;//tick kalles omtrent 60 ganger i sekundet så 1sek/60ticks =0.01666 sek/tick
			spcApp.entities().forEach(ent ->{//strømmer gjennom listen over alle enheter som befinner seg på spillbrettet
				switch (ent.getType()) {//har en switch/case på hva slags type enheten er
	
				case "enemybullet": //om enheten er en kule som g´har blitt skutt av en fiende
					enemyBullet(ent,player);
					break;
					
				case "playerbullet"://om enheten er er en kule skutt av spilleren
					playerBullet(ent,spcApp.entities());//denne funksjonen tar inn hele lista igjen for å igjen strømme gjennom den
						break;//og sjekke om kula intersecter med noen som helst andre enheter
	
				case "enemy"://dette er logikken som tilfeldig generer skudd fra fiendene
					if (time> 2) {//annehvert sekund skyter fiende, det gir en passende vanskelighetsgrad
						if (Math.random() < 0.5) {//det er 50% individuell sjans for at en fiende skyter 
							spcApp.shoot(ent);//skytefunksjonen må vite hvem som skøyt
						}
					}
					break;	
				}//om noen av enhetene har blitt "drept" i funksjonene player eller enemybullet
				if (ent.isDead()) {//fjernes entitien fra rooten
					spcApp.removeEnt(ent);//kaller på appen for å ta seg av det grafiske
				}
			});
		//System.out.println(entities().size());
		if (spcApp.entities().size() == 2 || player.isDead()) {//dette er logikken for når det er på tide å kalle next wave
			appController.fireWaveCount(waveCount);//sender et signal til kontrolleren om å lagre at spilleren har nådd neste nivå
			if (player.isDead()) {//om spilleren er død
				waveCount=0;//sett wavecount til 0 og clear game kalles
				spcApp.clearGame();
			}
			if (waveCount > 5) {//om spilleren vinner spillet skal han få en gratualsjonstekst og spillet pauses
				spcApp.drawWinningText();
			}
			else {//så lenge spilleren ikke har vunnet kalles next wave
			 nextWave();
			}	
		}
		if (time > 2) //trenger ikke krøllparantes om det kun er en linje under if´en
			time = 0;//resetter tiden til null etter to sekunder
	}
//tick hjelpefunksjoner ////////////////////////////////////////
	public void enemyBullet(GameEntity ent,GameEntity player) {//denne kunne fint ha vært private men jeg må ha noe å teste, får ikke testa noe om alt er private
		ent.moveDown();//beveger kula fem piksler nedover
		if (ent.getBoundsInParent().intersects(player.getBoundsInParent())) {//sjekker om den har truffet noe
			player.chopHealth();//i det tilfelle tar vi en del av spillerens health
			
			if (player.getHealth()==0) {//om alt livet til spilleren har blit tatt setter vi spillerens tilstand til å være død
				player.kill();
			}
			ent.kill();//setter kulas tilstand til å være død om den treffer spilleren
		}
		if (ent.getPosY() > gameHeight) {//om kula ikke traff noe og går ut av spillskjermen fjernes den ved å drepe den
			ent.kill();
		}
	}
	public void playerBullet(GameEntity ent, List<GameEntity> ents) {//siden spillerens kule har et variabelt antall fiender den kan treffe må vi ha en oversikt over alle fiendene
		ent.moveUp();
		ents.stream().filter(e -> e.getType().equals("enemy")).forEach(enemy -> {//her sorter jeg ut alle fiendene fra lista over alle spillenheter
			if(ent.getBoundsInParent().intersects(enemy.getBoundsInParent())) {//logikken for om kula treffer en fiende
			enemy.kill();//dersom den traff setter vi tilstanden til fienden til død, det samme gjelder kula
			ent.kill();//om vi ikke hadde drept kula hadde fortsatt gjennom fienden og risikert å treffe en overliggende fiende og man kunne ha fått en dobbelKill, det hadde vært for op
			}
		});
		if (ent.getPosY() <0) {//om kula ikke treffer noen fiender og beveger seg ut av skjermen fjernes den
			ent.kill();
		}
	}
	
	public void nextWave() {//logikken for hvor de neste fiendene skal spawne
		waveCount++;//husk å oppdatere nivåcounten så vi kan genere et høyrere nivå
		spcApp.drawWaveText();//appen må informere spilleren om at ny wave er nådd
		switch (waveCount) {//bruker en switchcase, det er noe repeterende kode her og det er sikkert en kortere metode for å gjøre det samme men 
		case 1://istdet for å tenke ut en dynamisk løsning med en haug med løkker har jeg bare gjort det enkelt
			generateRowOfEnemies(100, 1);			
			break;
		case 2:
			generateRowOfEnemies(100, 5);
			generateRowOfEnemies(150, 6);
			break;
		case 3:
			generateRowOfEnemies(100, 5);
			generateRowOfEnemies(150, 6);
			generateRowOfEnemies(200, 5);
			break;
		case 4:
			generateRowOfEnemies(100, 5);
			generateRowOfEnemies(150, 6);
			generateRowOfEnemies(200, 5);
			generateRowOfEnemies(250, 6);
			break;
		case 5:
			generateRowOfEnemies(100, 5);
			generateRowOfEnemies(150, 6);
			generateRowOfEnemies(200, 5);
			generateRowOfEnemies(250, 6);
			generateRowOfEnemies(300, 5);
			break;
		default:
			break;
		}
	}
	private void generateRowOfEnemies(int posY,int numberOfEnemies) {//denne funksjonen regener ut individuell posisjon på hver eneste enemy og
		int spacing = (int) gameWidth/(numberOfEnemies+1);//sier til space app hvor fienden skal tegnes
		for (int i = 0; i < numberOfEnemies; i++) {
		spcApp.drawEnemy(spacing + spacing*i-32/2,posY);	
		}
	}
	public int getWaveCount() {//getter for spaceapp så den kan hente ut hvilket tall det skal stå i wavetext
		return waveCount;
	}
	public void setSpaceInvadersApp(SpaceInvadersApp app) {//funksjon som app kaller en gang i oppstart slik at vi ikke trenger å sende inn (this)(appen) i tickfunksjonen
		this.spcApp=app;
	}
	
}
