package SpaceInvadersPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SpaceInvadersApp extends Application {
	
///// felter/////////////////////////////////////////////////////////////////////////////////////////////////
	//til grafikk
	private Pane root = new Pane();
	/*
	 * Dette er lerretet til spillet, alt som finnes i spillet er "malt" på lerret på en bestemt posisjon
	 * i dette spillet er lerretet en pane og ikke en parent som en fxml i utgangspunktet gir, det er fordi det er tilnærmet umulig å lage et 
	 * dynamisk sprite-basert spill med et ubestemt antall spillelementer i en Parent fordi nodene (children) i en parent ikke er
	 * modifiserbare. Det er de derimot om man bruker en Pane, i en pane kan man aksessere et hvilket som helst children og gjøre hva man vil med
	 * det. Det betyr at selve spillgrafikken ikke er fxml, men det har jeg løst ved å lage en spillmeny i fxml etter å ha forhørt
	 * meg med studassen jeg har fått tildelt.   
	 */
	private final int gameWidth = 500, gameHeight = 600;//forhåndsbestemt størrelse på spillskjermen
	private GameEntity player,healthBar;// to enheter som alltid er på spillbrettet, det er enklest å ha de instansiert for å kunne aksessere de raskt uten å itere gjennom en liste og lete etter "player"
	private Text infoText,waveText;//en instans av teksten som vises på skjerm
	private SpriteHandler spriteHandler= new SpriteHandler();//en instans av klassen som forer grafikken med bilder i stedet for kun solid fill av en farge
	private List<ImagePattern> healthbarList;//en oversikt over alle bildene healthbar kan ha
	//for å sette og bytte scenes til stagen
	private Parent menuRoot;//Parenten til menyScenen
	private Scene menuScene,gameScene;//en instans av begge scenene
	private AppController appController; // må ha controlleren for å kunne bytte mellom scenes i staget, samt oppdatere om nytt nivå nådd, kontrolleren kan ikke lese det selv
	private boolean gameIsPaused=true;//dette er for animation timeren
	private SpaceInvaderLogic spcLogic;//en instans av klassen som styrer logikken
		
///// Metoder/////////////////////////////////////////////////////////////////////////////////////////////////
	public List<GameEntity> entities(){//ja det er en metode, men jeg bruker den som et public felt, grunnen til at det er public er at logikken skal kunne aksessere og holde oversikt over spillelementene
		return root.getChildren().stream()//grunnen til at jeg gjør dette slik er for å kunne filtrere andre nodes enn gameentity og den blir dynamisk uten å bruke add/remove
				.filter( e -> e instanceof GameEntity)
				.map(ent -> (GameEntity)ent).collect(Collectors.toList());
	}
///// Metoder får sette igang spill, controller og visuals ///////////////////////////////////////////////////
	@Override
	public void start(Stage stage){//startfunksjonen som initialiserer spillet etter at launch kalles i main helt nederst
		System.out.println("spaceinvaders app started");
		createGameContent();//funksjonen tegner alt av grafikk og innhold som trengs når brukeren åpner programmet
		
		stage.setScene(menuScene);//her setter vi menyen til å vises etter at create game content har lagd scenen
		stage.show();//må eksplisitt si at den skal vises
		appController.setGame(this);//setter en instans av dette spillet i kontrolleren for å enkelt kunne kalle på hverandre og bytte scenes og varsle om endirnger i nivå nådd
		appController.setTheStage(stage);//legger inn en instans av stagen som vi oppretter i create gamecontent i controlleren så controlleren kan styre hvilken scene som skal være i stagen
	}
	
	private void createGameContent() {//funksjonen tegner alt av grafikk og innhold som trengs når brukeren åpner programmet
		drawInitalGrapchics();//tegner spill grafikken og menygrafikken, samt lagre tilstanden i scenes
		connectLogic();//kobler til logikklassen som hele tiden evaluerer posisjonene til enhetene på spillet
		AnimationTimer timer = new AnimationTimer() {//denne AnimationTimeren er hva som driver spillet, denne skal i utgangspunktet
			@Override//ticke 60 ganger i sekundet
			public void handle(long now) {//dette er kun syntaks
				if (!gameIsPaused) 
					tick();			//kaller ticken omtrent 60 ganger i sekundet så lenge spillet ikke er pauset
			}};
		timer.start();				//man må eksplisitt starte timeren
		spcLogic.nextWave();		//sier til logikken at grafikken har lastet og det er klart til å ta i mot første wave med fiender
	}
	
	private void drawInitalGrapchics() {
		root.setPrefSize(gameWidth,gameHeight);//setter størrelse til spillskjermen(500x600)
		drawBackground();//disse sier seg selv
		drawPlayer();
		fillHealthBarList();
		drawHealthBar();
		drawInfoText();//her brukes en fxml-loader som laster en fxml-fil og henter ut rooten (Parent) slik at vi kan velge root manuelt
		try {//en annen viktig ting med loaderen er at controlleren initialiseres og ikke er null når vi senere ønsker å lagre en instans av den, for å kunne bytte scenes
			FXMLLoader loader = new FXMLLoader(getClass().getResource("SpaceInvadersMenu.fxml"));
			menuRoot = (Parent)loader.load(); //lagrer rooten i appens tilstand
			appController = (AppController)loader.getController();//lagrer controlleren i appens tilstand
		} catch (Exception e) {
			System.out.println("// load MenuRoot failed. //");
			e.printStackTrace();
		}
		menuScene = new Scene(menuRoot);//lager en scene på menyrooten
		Scene scene = new Scene(root);//lager en scene på spillrooten
		setcontrollers(scene);//instansierer tastaturknappene som skal kunne brukes i spillet, kunne ha lagd denne funksjonen i controlleren for å være noe mer i henhold
		//til MVC men, å sette kontrollene slik krever en scene, og då må jeg også passe scenen til kontrolleren, og det blir bare å flytte den for å flytte den uten noen annen praktisk hensikt
		//spurte også studass om det hadde noen hensikt å flytte denne funksjonen, litt vanskelig å tyde men han ingen spesiell mening om det så jeg har bare latt den stå her i appen
		this.gameScene = scene;		
	}
	
	private void drawBackground() {
		try {
			ImageView imgview = new ImageView(spriteHandler.getSprite("SpaceBackgrund"));//tar inn en bakgrunn fra spritehandleren og setter den
			root.getChildren().add(imgview);
						
		} catch (IllegalArgumentException e) {
			System.out.println("//draw background failed, due to wrong picture name //");
			e.printStackTrace();
		}
	}
	private void drawPlayer() {//tegner spilleren på brettet, dette er et rektangel
		try {
			player = new GameEntity("player", 235, 560, 32, 32, Color.BLUE);//den får en farge i tilfelle grafikken skulle feile så spillet fortsatt er spillbart
			try {
				player.setFill(new ImagePattern(spriteHandler.getSprite("TDTSpaceShip")));		//forsøker å sette et bilde istedet for farge		
			} catch (IllegalArgumentException e) {
				System.out.println("// could not draw player graphics due to illegal picture name");
				e.printStackTrace();}
			root.getChildren().add(player);
		} catch (IllegalArgumentException e) {
			System.out.println("// Draw player failed due to illegal coordinates //");
			e.printStackTrace();
		}
	}
	private void fillHealthBarList() {//fyller en liste med oversikt over de forskjellige healthbarene
		healthbarList = new ArrayList<>();
		try {
			healthbarList.add(new ImagePattern(spriteHandler.getSprite("EmptyHealth")));
			healthbarList.add(new ImagePattern(spriteHandler.getSprite("OneThirdHealth")));
			healthbarList.add(new ImagePattern(spriteHandler.getSprite("TwoThirdsHealth")));
			healthbarList.add(new ImagePattern(spriteHandler.getSprite("FullHealth")));
			
		} catch (IllegalArgumentException e) {
			System.out.println("// Fille healthbar with images failed due to wrong picture name");
			e.printStackTrace();
		}
	}
	private void drawHealthBar() {//lager healthbargrafikken, dette er et rektangel som farges med bilder
		try {
		healthBar  = new GameEntity("neutral",3, 3, 96, 32,Color.TRANSPARENT);//denne er transparent, gir ikke mening å bare ha en svart boks i hjørnet om bildet feiler 
		try {
			healthBar.setFill(healthbarList.get(0));			
		} catch (IndexOutOfBoundsException e) {
			System.out.println("// healthbar is transparent due to index out of bounce");
		}
		root.getChildren().add(healthBar);//må huske å eksplisitt si at enheten skal legges til på panen
		}catch (IllegalArgumentException e) {
			System.out.println("//Draw healthbar failed, illegal xy position //");
			e.printStackTrace();
		}
	}
	private void drawInfoText() {//tegner informasjonsteksten som vises over fiendene mens man spiller
		infoText = new Text("press p or ESC to go back");
		infoText.setTranslateX(gameWidth/2-90);
		infoText.setTranslateY(30);
		setTextStyle(infoText);
		root.getChildren().add(infoText);
	}
	private void setcontrollers(Scene scene) { 
		scene.setOnKeyPressed(key->{//instansierer kontrollene
			switch (key.getCode()) {
			case LEFT:
			case A://left Arrow eller A beveger spilleren til venstre 
				player.moveLeft();
				break;
			case RIGHT:
			case D://Right Arrow eller d flytter spilleren mot høyre
				player.moveRight();//med andre ord kan bruker velge om mellom wasd eller arrows
				break;
			case SPACE://skyter med space
				shoot(player);//skytefunksjonen tar inn hvem som skyter for å avgjøre kulas egenskaper
				break;
			case P:
			case ESCAPE://både P og Esc kan brukes
				gameIsPaused = true;//pauser spillet
				appController.switchToMenuScene();//funksjon får å bytte til menyscenen
				appController.fireWaveCount(spcLogic.getWaveCount());//varsler controlleren om at nytt nivå er nådd
				break;
			default:
				break;
			}
		});
	}
	private void connectLogic() {//denne funksjonen oppretter logikken og setter en instans av seg selv i logikken så den enkelt kan kalle på app(denne klassen)
		spcLogic = new SpaceInvaderLogic();
		spcLogic.setSpaceInvadersApp(this);
	}	
	
/////  Metoder som ikke handler om oppsettet //////////////////////////////////////////////////////////////////////////
	private void tick() {//kunne nok lagt disse direkte i animation timer men her er det god mulighet for utvidelse
		spcLogic.tick(player, appController);
		updateHealthBarGraphic();
	}
	public void updateHealthBarGraphic(){
		try {//oppdaterer grafikken på healthbaren med hensyn på antall liv spilleren har igjen
			healthBar.setFill(healthbarList.get(player.getHealth()));			
		} catch (IndexOutOfBoundsException e) {
			System.out.println("// could not update healthbargraphics due to index out of bounce");}
	}
	public void drawWaveText() {//funksjon får tegne teksten som sier hvilken wave man er på
		root.getChildren().remove(waveText);//må først fjerne gammel tekst, ellers blir det tekst opp hverandre
		waveText = new Text("Wave: "+String.valueOf(spcLogic.getWaveCount()));
		setTextStyle(waveText);//setter stilen på teksten
		waveText.setTranslateX(gameWidth/2-40);//setter posisjonen til teksten
		waveText.setTranslateY(45);
		root.getChildren().add(waveText);//legger til slutt teksten til i rooten
	}
	public void drawWinningText() { //relativt lik som draw wavetekst men litt annet innhold
		root.getChildren().remove(waveText);
		waveText = new Text("Congratulations! Your hav WON the game!");
		setTextStyle(waveText);
		waveText.setTranslateX(gameWidth/2-60);
		waveText.setTranslateY(45);
		root.getChildren().add(waveText);
		gameIsPaused=true;
	}

	public void drawEnemy(int x, int y) {//funksjon for å putte en ny fiende på rooten
		try {
			GameEntity enemy = new GameEntity("enemy",x,y, 32, 32, Color.RED);
			try {//prøver å gi fienden fiendebildet fra resources
				enemy.setFill(new ImagePattern(spriteHandler.getSprite("Enemy")));				
			} catch (IllegalArgumentException e) {
				System.out.println("// could not draw enemy due to illegal picture name //");
				e.printStackTrace();
			} 
			root.getChildren().add(enemy);//legger fienden til på rooten
		} catch (IllegalArgumentException e) {
			System.out.println("// could not draw enemy due to illegal coordinates //");
			e.printStackTrace();
		}
	}
	//brukes for å tilbakestille antallet spillere til wave 1
	public void clearGame() throws IllegalArgumentException{
		entities().forEach(e->root.getChildren().remove(e));//skulle kanskje tro denne også fjernet bakgrunnen, men nei, merk filteret i entities
		drawPlayer();//oppretter ny player og healthbargrafikk
		drawHealthBar();
	}
	//funksjonen tar i GameEntities som skyter
	public void shoot(GameEntity ent) {//funksjon for å spawne en bullet, kan kalles av både spiller og enemies
		try {//avhengig av hvem som skyter går bulleten opp eller ned
			GameEntity bullet = new GameEntity((String) ent.getType()+"bullet", ent.getTranslateX()+15, ent.getTranslateY(), 7, 17, Color.BLACK);
			try {//her setter vi fargefyllet til et bilde
				bullet.setFill(new ImagePattern(spriteHandler.getSprite("Bullet")));
			} catch (IllegalArgumentException e) {
				System.out.println("// set sprite to bullet failed, due to illegal picture name");}
			root.getChildren().add(bullet);	//legger kula til rooten		
		} catch (IllegalArgumentException e) {
			System.out.println("// Shoot function failed due to illegal coordinates //");
			e.printStackTrace();
		}
	}
///// Små Metoder /////////////////////////////////////////////////////////////////////////////////////////////////
	public void unPauseGame() {
		System.out.println("toggled unPauseGame");
		this.gameIsPaused = false;
	}
	public void removeEnt(GameEntity ent) {//fjerner en entity fra rooten
		if (root.getChildren().contains(ent)) {
			root.getChildren().remove(ent);			
		}
	}
	private void setTextStyle(Text text) {//hjelpefunksjon får å slippe å repetere hvordan tekststilen skal være
		text.setFont(Font.font("Arial",FontWeight.SEMI_BOLD,FontPosture.ITALIC,16));
		text.setFill(Color.GRAY);
	}
	public Scene getScene() {//funksjon så controlleren kan hente scene for å bytte
		return this.gameScene;
	}
	public static void main(String[] args) {//kun for å launche appen
		launch(args);
	}

}
