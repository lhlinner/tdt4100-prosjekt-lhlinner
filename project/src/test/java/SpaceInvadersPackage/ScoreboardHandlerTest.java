package SpaceInvadersPackage;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ScoreboardHandlerTest {
	ScoreboardHandler handler;
	//private double epsilon = 0.000001d;
	@BeforeEach
	public void setup() {
		handler = new ScoreboardHandler();
	}
	@Test
	public void loadAnotherFileTest() {
		handler.loadGameResource("TestScoreboardHandlerReadFile");//laste inn en fil med bestemte verdier
		assertTrue(handler.checkIfScoreboardLoadedToHashmap("Best"));//funksjonen returner en nboolean om "best" er lagret i objektets tilstand
	}
	@Test
	public void getTopThreeTextTest(){
		handler.clearScoreboard();//fjerner hva som evt måtte ligge der fra før
		handler.loadGameResource("TestScoreboardHandlerReadFile");//laste inn en fil med bestemte verdier
		assertEquals("Top three players \n"
				+ "Rank|Wave|Name \n"
				+ " 1.        3      Best\n"
				+ " 2.        2      Astronaut\n"
				+ " 3.        2      Romskip\n"
				,handler.getTopTreeText() );//sjekker at top three text er som forespeilet
	}
	
	@Test
	public void registerUserTest() {//tester teksten som skal komme opp når man trykker på knappen ved siden av inntastingsfeltet
		handler.registerUser("TestFive");
		assertTrue(handler.checkIfScoreboardLoadedToHashmap("TestFive"));
	}
	@DisplayName(value = "toggle wave count test one via register user") 
	@Test
	public void toggleWaveCountTestOne() {
		handler.clearScoreboard();
		handler.loadGameResource("TestScoreboardHandlerReadFile");
		handler.registerUser("TestSix");
		handler.updateUserWave("TestSix", 5);
		assertEquals("Top three players \n"
				+ "Rank|Wave|Name \n"
				+ " 1.        5      TestSix\n"
				+ " 2.        3      Best\n"
				+ " 3.        2      Astronaut\n"
				,handler.getTopTreeText() );
		
	}
	@DisplayName(value="toggle wave count test two, not via register user")
	@Test
	public void toggleWaveCountTestTwo() {
		handler.clearScoreboard();
		handler.updateUserWave("TestSeven", 2);
		assertTrue(handler.checkIfScoreboardLoadedToHashmap("TestSeven"));
	}
	@Test
	public void testWriteToFile() {
		//Sette opp kjent innhold:
		handler.clearScoreboard();
		handler.loadGameResource("TestScoreboardHandlerReadFile");
		assertTrue(handler.checkIfScoreboardLoadedToHashmap("Astronaut"));//påser at det finnes noe å lagre
		//lagre innholdet
		String filename = "TestScoreboardHandlerWriteFile";
		handler.saveResource(filename);
		//slette innholdet så det ikke er noe i tilstanden men må være lagret om det skal være mulig å finne igjen
		handler.clearScoreboard();
		assertFalse(handler.checkIfScoreboardLoadedToHashmap("Astronaut"));//påser at ting faktisk ble slettet
		//laster inn det vi skrev til filen og sjekker om astronaut er tilbake
		handler.loadGameResource(filename);
		assertTrue(handler.checkIfScoreboardLoadedToHashmap("Astronaut"));
	}
}
